<?php
require_once "bitacora.php";

header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header('Access-Control-Allow-Headers', 'Content-Type');

session_start();

abstract class Buscar
	{
		public static function run()
		{
            $fecha_ini = isset($_POST["fecha_inical"]) ? $_POST["fecha_inical"] : "";
            $fecha_fin = isset($_POST["fecha_final"]) ? $_POST["fecha_final"] : "";
            $porFecha = isset($_POST["porfecha"]) ? $_POST["porfecha"] : "";

            $query = "SELECT users.user,bitacora.action,bitacora.date,bitacora.hour FROM users
                        INNER JOIN bitacora ON bitacora.user_id = users.id WHERE 1";

            if($porFecha == "true")
            {
                $query .= " AND bitacora.date BETWEEN '$fecha_ini' AND '$fecha_fin' ";
            }

            // echo json_encode([
            //     'data' => $query,
            //     'porFecha' => $porFecha,
            //     'post' => $_POST
            //     ]);
            // exit;
           
            $bitacora = new Bitacora();
            
            
            $bitacora->search($query);
		}
		
	}
	Buscar::run();
?>