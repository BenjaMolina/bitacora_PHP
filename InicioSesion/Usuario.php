<?php

require_once 'connection.php';

class Usuario {

	private $_id;
	private $_user;
	private $_pass;

	public function __construct() {
		$this->_id = 0;
		$this->_user = "";
		$this->_pass = "";
	}

	//Métodos para entrada de datos
	public function setID($id) {
		$this->_id = $id;
	}

	public function setUser($user) {
		$this->_user = $user;
	}

	public function setPass($pass) {
		$this->_pass = $pass;
	}

	//
	//Métodos para obtención de datos
	//

	public function getID() {
		return $this->_id;
	}

	public function getUser() {
		return $this->_user;
	}

	public function getPass() {
		return $this->_pass;
	}


	//
	//Métodos para alteración del comportamiendo del objeto
	//

	//Verifica si el usuario intentando loguearse realmente existe.
	public function getUserLogin() {
		$userLogin = new Usuario();

		try{
			$db = Connection::getConnection();

			$sql = "SELECT id, user, pass FROM users WHERE user=? and pass=?";
	
			if (!$stmt = $db->prepare($sql)) {
				throw new Exception (' '.$db->error);
			}

			$stmt->bind_param("ss", $this->getUser(), $this->getPass());

			$stmt->execute();

			if (!$stmt->bind_result($id, $user, $pass)) {
				throw new Exception (' '.$db->error);
			}

			while ($stmt->fetch()) {			
				$userLogin->setID($id);
				$userLogin->setUser($user);
				$userLogin->setPass($pass);
				
				return $userLogin;
			}

			return null;
		}
		catch(Exception $e){
			echo "Archivo: ".$e->getFile()." Línea: ".$e->getLine()." Descripción: ". $e->getMessage();
	  		return false;
		}
	}
}
?>