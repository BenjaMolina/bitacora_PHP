<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />

     <style>
    body
    {
        margin:0;
        padding:0;
        background-color:#f1f1f1;
    }
    .box
    {
        width:1270px;
        padding:20px;
        background-color:#fff;
        border:1px solid #ccc;
        border-radius:5px;
        margin-top:25px;
    }
    </style>
    
</head>
<body>
    <div class="container box">
        <h1 class="text-center">Bitacora</h1>
        <br />
        <div class="row">
            <div class="input-daterange">
                <div class="col-md-4">
                    <label for="">Fecha inicial</label>
                    <input type="date" onchange="filtroFecha()" name="fecha_inical" id="fecha_inical" class="form-control" />
                </div>
                <div class="col-md-4">
                    <label for="">Fecha final</label>
                    <input type="date" onchange="filtroFecha()" name="fecha_final" id="fecha_final" class="form-control" />
                </div>
            </div>
            <div class="col-md-4 checkbox">
                <label><input type="checkbox" id="porfecha">Por Fecha</label>
            </div>
        </div>
        <div class="table-responsive">
            
            <br />
            <table id="example" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Accion</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>User</th>
                        <th>Accion</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>

    <script src="script.js"></script>
    
</body>
</html>


<!-- <!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Inicio de Sesión</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">       
    </head>
    <body>    
        <h1>Iniciar Sesión</h1>

        <form action="Autentication.php" method="post">
            <div>
                <input type="text" name="user" placeholder="Nombre de usuario">                       
            </div>
            <div>
                <input type="password" name="pass" placeholder="Contraseña">                        
            </div>
            <div>
                <button type="submit" class="btn btn-primary btn-block btn-flat">Acceder</button>
            </div>
        </form>
    </body>
</html> -->
