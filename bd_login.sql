-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 10, 2018 at 04:32 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd_login`
--

-- --------------------------------------------------------

--
-- Table structure for table `bitacora`
--

CREATE TABLE `bitacora` (
  `id` int(11) NOT NULL,
  `action` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `hour` time DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bitacora`
--

INSERT INTO `bitacora` (`id`, `action`, `date`, `hour`, `user_id`) VALUES
(1, 'Inició sesión', '2018-09-06', '19:30:30', 1),
(2, 'Cerró sesión', '2018-09-06', '19:31:51', 1),
(3, 'Inició sesión', '2018-09-06', '19:49:01', 1),
(4, 'Inició sesión', '2018-09-07', '23:31:59', 1),
(5, 'Inició sesión', '2018-09-08', '00:58:37', 1),
(7, 'Cerró sesión', '2018-09-08', '01:04:25', 1),
(8, 'Cerró sesión', '2018-09-08', '01:04:47', 1),
(9, 'Cerró sesión', '2018-09-08', '01:07:03', 1),
(10, 'Inició sesión', '2018-09-08', '01:07:17', 1),
(11, 'Download', '2018-09-08', '01:19:08', 1),
(12, 'Download', '2018-09-08', '01:19:16', 1),
(13, 'Download', '2018-09-08', '01:20:07', 1),
(14, 'Cerró sesión', '2018-09-08', '01:31:50', 1),
(15, 'Inició sesión', '2018-09-08', '01:39:13', 1),
(16, 'Cerró sesión', '2018-09-08', '01:40:26', 1),
(17, 'Inició sesión', '2018-09-08', '01:46:18', 1),
(18, 'Cerró sesión', '2018-09-08', '01:46:20', 1),
(19, 'Inició sesión', '2018-09-08', '01:47:06', 1),
(20, 'Download', '2018-09-08', '01:47:08', 1),
(21, 'Download', '2018-09-08', '01:47:12', 1),
(22, 'Cerró sesión', '2018-09-08', '20:19:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` varchar(10) NOT NULL,
  `pass` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user`, `pass`) VALUES
(1, 'root', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_bitacora_users` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bitacora`
--
ALTER TABLE `bitacora`
  ADD CONSTRAINT `FK_bitacora_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
