<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>Bienvenido</title>      
</head>

<script>
	function exit (id)
		{
			var confirmation=confirm("¿Desea cerrar la sesión?");

			if(confirmation) {
				document.location="../close.php?id="+id;
				//document.location="../index.php";
			}
			else {
				return false;
			}
		}
</script>
<?php
session_start();

if (isset($_SESSION['user']) && $_SESSION['loggedin'] == 1) {
	?>
	

	<h1>Bienvenido <?php echo $_SESSION['user'] ?></h1>
	<br />
	<?php echo "<a href='#' onClick='exit(". $_SESSION['id'] .")'> Cerrar sesión </a>"; ?>
	

	<?php
} else {
   	echo "Esta página es solo para usuarios registrados.<br>";
   	echo "<br><a href='../index.php'>Login</a>";   
	exit;
}
?>

<body>

</body>
</html>