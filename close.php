<?php
require_once "bitacora.php";

header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header('Access-Control-Allow-Headers', 'Content-Type');

session_start();

abstract class Close
	{
		public static function run()
		{
			$id=$_REQUEST["id"];
			$_SESSION['user'] = null;
			$_SESSION['loggedin'] = false;
	
			

			$bitacora = new Bitacora();
            $bitacora->setAction('Cerró sesión');
            $bitacora->setIDUsuario($id);
			
			if($bitacora->insert()){
				echo json_encode([
					'status' => true,
					'logout' => true,
					'message' => 'Sesion cerrada con exito',
				]);
			}
			else{
				echo json_encode([
					'status' => false,
					'logout' => false,
					'message' => 'Sesion fallida',
				]);
			}
			
			
			exit;
			

            header('Location: index.php');
		}
		
	}
	Close::run();
?>