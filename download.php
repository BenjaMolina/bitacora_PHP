<?php
require_once "bitacora.php";

header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header('Access-Control-Allow-Headers', 'Content-Type');

session_start();

abstract class Download
	{
		public static function run()
		{
            $id=$_REQUEST["id"];
            
			$bitacora = new Bitacora();
            $bitacora->setAction('Download');
            $bitacora->setIDUsuario($id);
            
            if($bitacora->insert()){
                echo json_encode([
                    'status' => true,
                    'dowload' => true,
                    'message' => 'Download con exito',
                ]);
                
            }
            else{
                echo json_encode([
                    'status' => false,
                    'dowload' => false,
                    'message' => 'Download sin exito',
                ]);
                
            }
			exit;		
			

            header('Location: index.php');
		}
		
	}
	Download::run();
?>