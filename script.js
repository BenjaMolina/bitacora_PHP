let porfecha = document.getElementById('porfecha');
let fechaIni = document.getElementById('fecha_inical');
let fechaFin = document.getElementById('fecha_final');


fechaIni.disabled = true;
fechaFin.disabled = true;

porfecha.checked = false;

porfecha.addEventListener("change", function() {
    if (porfecha.checked) {
        fechaIni.disabled = false;
        fechaFin.disabled = false;

    } else {
        fechaIni.disabled = true;
        fechaFin.disabled = true;
    }
});

const confDataTable = () => {
    $('#example').DataTable({
        "language": {
            "lengthMenu": "Ver _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "info": "Var pagina _START_ de _END_ de _TOTAL_ entradas",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
}

let getData = () => {

    console.log(fechaIni.value);
    console.log(fechaFin.value);
    console.log(porfecha.checked);

    $('#example').DataTable().destroy();

    const datasend = JSON.stringify({
        "fecha_inical": fechaIni.value,
        "fecha_final": fechaFin.value,
        "porfecha": porfecha.checked
    });


    // fetch('buscar.php', {
    //         method: 'POST',
    //         headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    //         body: `fecha_inical=${fechaIni.value}&fecha_final=${fechaFin.value}&porfecha=${porfecha.checked}`
    //     })
    //     .then(res => res.json())
    //     .then(res => {

    //         var ttbody = document.querySelector('#example tbody');
    //         ttbody.innerHTML = "";

    //         if (res.data.length > 0) {

    //             let thead = document.querySelector('#example thead');
    //             let body = `<tbody>`;
    //             res.data.map(data => {
    //                 body += `<tr>
    //                             <td>${data[0]}</td>
    //                             <td>${data[1]}</td>
    //                             <td>${data[2]}</td>
    //                             <td>${data[3]}</td>
    //                         </tr>`;
    //             })

    //             body += `</tbody>`;

    //             thead.insertAdjacentHTML('afterend', body);

    //         }

    //         confDataTable();

    //     })
    $.post('buscar.php', {
            fecha_inical: fechaIni.value,
            fecha_final: fechaFin.value,
            porfecha: porfecha.checked,
        })
        .done(function(data) {

            let res = JSON.parse(data);
            console.log(res);

            var ttbody = document.querySelector('#example tbody');
            ttbody.innerHTML = "";

            if (res.data.length > 0) {

                let thead = document.querySelector('#example thead');
                let body = `<tbody>`;
                res.data.map(data => {
                    body += `<tr>
                                        <td>${data[0]}</td>
                                        <td>${data[1]}</td>
                                        <td>${data[2]}</td>
                                        <td>${data[3]}</td>
                                    </tr>`;
                })

                body += `</tbody>`;

                thead.insertAdjacentHTML('afterend', body);

            }

            confDataTable();
        })
        .fail(function(e) {
            console.log(e);
        });
}

let buscar = () => {
    console.log('buscar');
}

function filtroFecha() {
    console.log("ajsdjkash");
    if (fechaIni.value != "" && fechaFin.value != "") {
        getData();
    }

}

// $('.input-daterange').datepicker({
//     todayBtn: 'linked',
//     format: "yyyy-mm-dd",
//     autoclose: true
// });

getData();