<?php

header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header('Access-Control-Allow-Headers', 'Content-Type');


session_start();

require_once 'InicioSesion/Usuario.php';

abstract class Autentication {

    public static function run() {

        $user = new Usuario();
        $user->setUser($_POST["user"]);
        $user->setPass($_POST["pass"]);

        $userLogin = $user->getUserLogin();

        $_SESSION["loggedin"] = false;
            
        if ($userLogin != null) {

            $_SESSION["id"] = $userLogin->getID();
            $_SESSION["user"] = $userLogin->getUser();
            $_SESSION["pass"] = $userLogin->getPass();
            $_SESSION["loggedin"] = true;

            //////AQUÍ SE AGREGA EL REGISTRO DE LOGUEO EN LA BITÁCORA
            require_once 'bitacora.php';
            $bitacora = new Bitacora();
            $bitacora->setAction('Inició sesión');
            $bitacora->setIDUsuario($userLogin->getID());
            
            if($bitacora->insert()){
                echo json_encode([
                    'status' => true,
                    'loggin' => true,
                    'message' => 'Bienvenido',
                    'id' => $userLogin->getID(),
                    'user' => $userLogin->getUser(),
                    'pass' => $userLogin->getPass(),
                ]);
            }
            else{
                echo json_encode([
                    'status' => false,
                    'loggin' => false,
                    'message' => 'Error al guardar en la BD',
                    'id' => $userLogin->getID(),
                    'user' => $userLogin->getUser(),
                    'pass' => $userLogin->getPass(),
                ]);
            }

            
            exit;

            echo "<script> alert ('Bienvenido " . $userLogin->getUser() . " al Sistema de logueo')</script>";
            echo "<script> location.href='system/Bienvenido.php' </script>";                    
        } else {
            echo json_encode([
                'status' => false,
                'loggin' => false,
                'message' => 'El usuario no se encuentra asociado a ninguna persona',
                ]);
            exit;

            echo "<script> alert ('El usuario no se encuentra asociado a ninguna persona')</script>";
            echo"<script> location.href='./' </script>";
        }
    
    }

}

Autentication::run();
