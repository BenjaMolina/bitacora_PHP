<?php

require_once 'connection.php';

class Bitacora {

	private $_id;
	private $_action;
	private $_date;
	private $_hour;
	private $_idUsuario;

	public function __construct() {
		$this->_id = 0;
		$this->_action = "";
	}

////////////////////////////////////////////////////////

	public function getID() {
		return $this->_id;
	}

	public function getAction() {
		return $this->_action;
	}

	public function getDate() {	
		Bitacora::setDate();
		return $this->_date;
	}

	public function getHour() {
		Bitacora::setHour();
		return $this->_hour;
	}

	public function getIDUsuario() {
		return $this->_idUsuario;
	}

///////////////////////////

	public function setAction($action) {
		$this->_action = $action;
	}

	public function setIDUsuario($id) {
		$this->_idUsuario = $id;
	}

	private function setDate() {
		//$this->_date = 'DATE_FORMAT(NOW( ), "%H:%i:%S" )';
		//$this->_date = 'CURDATE()';
		$this->_date = date('Y-m-d');
	}

	private function setHour() {
		//$this->_hour = 'DATE_FORMAT(NOW( ), "%H:%i:%S")';
		//$this->_hour = 'curTime()';
		$this->_hour = date('H:i:s');
	}


//////////////////////////////////////////////////////////
	public function insert() {
		try
		{
			$db = Connection::getConnection();
			$sql = "INSERT INTO bitacora VALUES (?,?,?,?,?)";
			
			if(!$stmt = $db->prepare($sql))
				throw new Exception(' ' . $db->error);

			$stmt->bind_param("isssi", $this->getID(), $this->getAction(), $this->getDate(), $this->getHour(), $this->getIDUsuario());		

			if($stmt->execute()){
				return true;
			}
			else{
				throw new Exception(' ' . $db->error);
			}

			return false;

		}
		catch(Exception $e)
		{
			echo "Archivo: ".$e->getFile()." Línea: ".$e->getLine()." Descripción: ". $e->getMessage();
	 		return false;
		}
	}

	public function search($sql)
	{
		try
		{
			$db = Connection::getConnection();
			
			if(!$stmt = $db->prepare($sql))
				throw new Exception(' ' . $db->error);	

			$result = $db->query($sql);

			$number_filter_row = $result->num_rows;
			$recordsTotal = $number_filter_row;

			$data = array();

			if($result->num_rows > 0){

				while($row = $result->fetch_assoc()) {
					$sub_array = array();
					$sub_array[] = $row["user"];
					$sub_array[] = $row["action"];
					$sub_array[] = $row["date"];
					$sub_array[] = $row["hour"];
					$data[] = $sub_array;
				}

			}
			
			$output = [
				"draw"    => intval($_POST["draw"]),
				"recordsTotal"  =>  $recordsTotal,
				"recordsFiltered" => $number_filter_row,
				"data"    => $data
			];
			   
			echo json_encode($output);

			exit;

		}
		catch(Exception $e)
		{
			echo "Archivo: ".$e->getFile()." Línea: ".$e->getLine()." Descripción: ". $e->getMessage();
	 		return false;
		}

	}
}

?>