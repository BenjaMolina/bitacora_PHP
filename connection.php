<?php

class Connection {

	//Obtiene la conexión a la base de datos con el actual SGBD
	public static function getConnection() {
       return Connection::getConnectionInMySQL();    
    }


    //Conexión para base de datos en MySQL
    private function getConnectionInMySQL() {
    	$connection = new mysqli("localhost", "root2", "", "bd_login");    

        if ($connection->connect_errno) {
            throw new Exception(" " . $connection->connect_error);
        }
        $connection->set_charset("utf8");

        return $connection;
    }


    //Conexión para base de datos en SQL Server
    private function getConnectionInSQLServer() {    	

        //$serverName = "serverName\sqlexpress, 1542"; //Usa esta linea en caso de que cuentes con un puerto específico
        $serverName = "serverName\sqlexpress"; //serverName\instanceName

        $connectionInfo = array( "Database"=>"bd_login", "UID"=>"root", "PWD"=>"");
        $conn = sqlsrv_connect( $serverName, $connectionInfo);

        if( $conn ) {
             echo "Conexión establecida.<br />";
             return $conn;
        }else{
             echo "Conexión no se pudo establecer.<br />";
             die( print_r( sqlsrv_errors(), true));
        }
                   
    }
}

?>